/// PARAMETERS ///

const w = 860; // width
const h = 750; // height
const N = 30; // hexgrid size
const SF = 8 ; // scaling factor
const FS = 24; // font size
const S0CP = "⬡⯌⋅◦⟐−⌄."; // state-0 char pool (8 chars)
const S1CP = "+⯅⯆⬢⭙⯁⯍⯊⯋⍣⧧⦁⟁⩷⨳⨯∴∵⋄⋆∼⋈∻⎊↺⋄⏣⎈◬⊛⌘❆"; // state-1 char pool (32 chars)
const S0P = [0.05, 0.1, 0.3, 0.5, 0.7, 0.95]  // random ruleset state-0 probabilitys
const FR = 120; // target FPS


/// GLOBALS ///

let C0, C1; // display chars 
let RS; // ruleset
let G1, G2; // grids
let GF = false; // flip-flop for which grid to use
let CL = false; // click-log to prevent holding mousebutton
let IP = false; // info panel flip-flop



/// COORDINATE SYSTEM ///

let R, Q, S;
// Initialize hex dir vectors
function setupCoordConstants() {
  R = createVector(0, 1);
  Q = createVector(cos(TAU-(TAU/12)), sin(TAU-(TAU/12)));
  S = createVector(-Q.x, Q.y);
}

// Get x,y position from r,q hex position
function hexAt(r, q) {
  let rV = p5.Vector.mult(R, r);
  let qV = p5.Vector.mult(Q, q);
  let sV = p5.Vector.mult(S, ((0-r)-q));
  return p5.Vector.add(rV, p5.Vector.add(qV, sV));
}



/// RULESET ///

class RuleSet {
  rs = [];
  
  constructor(code) {
    // New random ruleset or decode ruleset from hex code
    if (code == undefined) {
      let sp = S0P[Math.floor(Math.random() * S0P.length)];
      for (let i=0; i<2**7; i++) this.rs[i] = (Math.random() < sp) ? 0 : 1;
    } else {
      let rsc = BigInt('0x' + code).toString(2).split('').map((x) => parseInt(x) || 0);
      this.rs = Array((2**7) - rsc.length).fill(0).concat(rsc);
    }
  }

  // Get next value of a cell from its value and the value of its six neighbors
  next(pr, pq, ps, o, nr, nq, ns) {
    return this.rs[pr + (pq*2) + (ps*4) + (o+8) + (nr*16) + (nq*32) + (ns*64)];
  }

  // Convert ruleset to hex code
  code() {
    return BigInt('0b' + this.rs.join('')).toString(16);
  }

  // Get display chars from ruleset
  displayChars() {
    let rsb = this.rs.join('');
    let c0 = S0CP[parseInt(rsb.substring(0, 3), 2) || 0];
    let c1 = S1CP[parseInt(rsb.substring(3, 8), 2) || 0];
    return [c0, c1];
  }

}



/// HEX GRID ///

class HexGrid {
  n = 0;
  cells = {};

  // Contruct a grid of size N
  constructor(n) {
    this.n = n;
    for (let r = -n; r <= n; r++) {
      this.cells[r] = {};
      let qs = max(-n, (-r)-n);
      let qe = min(n, (-r)+n);
      for (let q = qs; q <= qe; q++) {
        this.cells[r][q] = Math.floor(Math.random() * 2);
      }
    }
  }

  // Get value from cell[r, q]
  getCell(r, q) {
    if (this.cells[r] == undefined) return 0;
    if (this.cells[r][q] == undefined) return 0;
    return this.cells[r][q];
  }

  // Print grid cells
  dump() {
    console.log(this.cells);
  }

  // Draw to screen
  render() {
    for (let r = -this.n; r <= this.n; r++) {
      let qs = max(-this.n, (-r)-this.n);
      let qe = min(this.n, (-r)+this.n);
      for (let q = qs; q <= qe; q++) {
        let pos = hexAt(r*SF, q*SF);
        let c;
        if (this.cells[r][q] == 0) {
          fill(0, 255, 180);
          c = C0;
        } else {
          fill(255);
          c = C1;
        }
        
        text(c, pos.x, pos.y);
      }
    }
  }

  // Calculate next generation
  next(prev, rs) {
    for (let r = -this.n; r <= this.n; r++) {
      let qs = max(-this.n, (-r)-this.n);
      let qe = min(this.n, (-r)+this.n);
      for (let q = qs; q <= qe; q++) {
        let pr = prev.getCell(r+1, q);
        let pq = prev.getCell(r, q+1);
        let ps = prev.getCell(r+1, q-1);
        let o = prev.getCell(r, q);
        let nr = prev.getCell(r-1, q);
        let nq = prev.getCell(r, q-1);
        let ns = prev.getCell(r-1, q+1);
        this.cells[r][q] = rs.next(pr, pq, ps, o, nr, nq, ns);
      }
    }
  }

}



/// SIMULATION ///

// Populate grids with new random values
function randGrid() {
  G1 = new HexGrid(N);
  G2 = new HexGrid(N);
}

// Set ruleset from hex code, randomize grid, and set display chars
function setRuleset(code) {
  randGrid();
  RS = new RuleSet(code);
  [C0, C1] = RS.displayChars();
}

// Generate new random ruleset, randomize grids, and set display chars
function randRuleset() {
  randGrid();
  RS = new RuleSet();
  [C0, C1] = RS.displayChars();
  window.location.replace('#' + RS.code());
}

// Calculate next grid-state and render grid
function next() {
  clear(0);
  if (GF) {
    G1.next(G2, RS);
    G1.render();
  } else {
    G2.next(G1, RS);
    G2.render();
  }
  GF = !GF;
}



/// INTERFACE ///

// Keyboard Interface
function keyPressed() {
  // Space for new random ruleset
  if (key === ' ') randRuleset();
}

// Mouse Interface
function mouseInterface() {
  // check if mouse is even in the sketch
  if ((pmouseX > 0) && (pmouseX < w) && (pmouseY > 0) && (pmouseY < h)) {
    if (mouseIsPressed && !CL) {
      // left-click for new ruleset
      if (mouseButton === LEFT) randRuleset();
      // right-click for new random grid
      else if (mouseButton === RIGHT) randGrid();
      CL = true;
    }
    if (!mouseIsPressed && CL) CL = false;
  }
}

// Set the ruleset from the hex code in the url hash
function setRulesetFromHash() {
  setRuleset(window.location.hash.substring(1))
}

// Toggle info panel
function toggleInfo() {
  let info = document.getElementById('info');
  let canvas = document.getElementsByClassName('p5canvas')[0];
  let qmark = document.getElementById('qmark');
  if (!IP) {
    info.style = 'opacity: 1;';
    canvas.style = 'opacity: 0.2;';
    qmark.style = 'opacity: 1;';
    IP = true;
  } else {
    info.style = 'opacity: 0;';
    canvas.style = '';
    qmark.style = '';
    IP = false;
  }
}



/// MAIN ///

// Preload
let Font;
function preload() {
  Font = loadFont('JuliaMono-Bold.otf');
}

// Initialization
function setup() {
  setupCoordConstants(); // setup coord basis vectors
  createCanvas(w, h, WEBGL); // init canvas
  // disable standard right click behavior on canvas and info panel
  document.getElementsByClassName("p5Canvas")[0].addEventListener("contextmenu", (e) => e.preventDefault());
  document.getElementById("info").addEventListener("contextmenu", (e) => e.preventDefault());
  addEventListener("hashchange", setRulesetFromHash);
  document.getElementById("qmark").addEventListener("click", toggleInfo);
  colorMode(HSB, 255); // set colorMode
  noStroke(); // no strokes pls
  fill(255);
  textSize(FS);
  textFont(Font);
  if (window.location.hash == '') randRuleset();
  else setRulesetFromHash();
}

// Frame rendering
let LF = 0; // last frame time
function draw() {
  if ((millis() - LF) > (1000 / FR)) {
    next();
    LF = millis();
  }
  mouseInterface();
}



